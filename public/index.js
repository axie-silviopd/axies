const getAxieByRonin = async (ronin) => {
  await axios
    .post("https://graphql-gateway.axieinfinity.com/graphql", {
      operationName: "GetAxieBriefList",
      variables: {
        auctionType: "All",
        owner: "0x" + ronin,
      },
      query:
        "query GetAxieBriefList($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String, $filterStuckAuctions: Boolean) {\n  axies(\n    auctionType: $auctionType\n    criteria: $criteria\n    from: $from\n    sort: $sort\n    size: $size\n    owner: $owner\n    filterStuckAuctions: $filterStuckAuctions\n  ) {\n    total\n    results {\n      ...AxieBrief\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieBrief on Axie {\n  id\n  name\n  stage\n  class\n  breedCount\n  image\n  title\n  battleInfo {\n    banned\n    __typename\n  }\n  auction {\n    currentPrice\n    currentPriceUSD\n    __typename\n  }\n  parts {\n    id\n    name\n    class\n    type\n    specialGenes\n    __typename\n  }\n  __typename\n}\n",
    })
    .then((response) => {
      const axies = response.data.data.axies.results

      axies.map((axie) => {
        let color = ""
        let logo = ""

        switch (axie.class) {
          case "Bird":
            color = "bird"
            logo = `<path d="M9.745 12.248a4.564 4.564 0 01-1.948.837c-.424.076-.857.046-1.278-.036-.386-.074-.86-.148-1.193-.36-.142-.089-.588.97-.645 1.075-.301.554-.783 1-1.357 1.238-.026.011-.056.021-.081.01-.029-.014-.038-.05-.043-.082-.088-.57.1-1.144.433-1.606.085-.117 1.03-1.065.876-1.168-.328-.221-.586-.626-.809-.949-.244-.353-.445-.737-.546-1.157a4.562 4.562 0 01-.014-2.12c.061-.286.246-.661.499-.824.068-.043.487.468.533.519.277.307.504.588.86.818L4.387 6.78c-.04-.102-.08-.212-.058-.32a.579.579 0 01.123-.222c1.52-1.988 3.078-4.134 5.606-4.845.189-.053 2.254-.687 2.36-.452.556 1.222.73 2.593.585 3.923-.128 1.167-.61 2.34-1.004 3.407-.275.743-.846 1.401-1.23 2.096a.58.58 0 01-.154.202c-.09.064-.207.07-.317.076l-1.782.075c.354.234.702.33 1.094.46.065.022.702.2.69.28-.048.297-.317.616-.555.788"></path>`
            break
          case "Plant":
            color = "plant"
            logo = `<path d="M14.205 4.357c-.796-.634-1.882-.941-2.89-.74C9.764 3.926 8.53 5.31 8 6.757c-.53-1.447-1.764-2.831-3.314-3.14-1.009-.201-2.095.106-2.891.74C.847 5.112.5 6.291.836 7.45c.255.879 1.11 1.204 1.933 1.364.912.178 1.906.33 2.617.997a4.745 4.745 0 011.233 1.946c.073.218.137.44.19.665.049.203.056.415.096.62.036.19.112.106.19.01.392-.485.692-1.08.905-1.696.213.616.513 1.21.905 1.695.078.097.154.18.19-.01.04-.204.048-.416.096-.619.053-.224.117-.447.19-.665a4.745 4.745 0 011.234-1.946c.71-.666 1.703-.82 2.616-.997.823-.16 1.678-.485 1.933-1.364.335-1.16-.011-2.338-.959-3.093"></path>`
            break
          case "Aquatic":
            color = "aquatic"
            logo = `<path d="M15.036 5.73c-.136-.615-.329-1.207-.989-.985-.3.102-.578.285-.843.47a8.114 8.114 0 00-1.82 1.777c-.646-1.22-1.717-2.15-2.73-2.786-1.575-.99-3.155-1.12-4.78-.239C2.5 4.712 1.326 6.03.94 7.717a2.81 2.81 0 00-.051.304c.012.1.027.202.05.304.387 1.686 1.562 3.005 2.935 3.75 1.625.88 3.205.751 4.78-.24 1.013-.636 2.084-1.565 2.73-2.786a8.108 8.108 0 001.82 1.776c.265.186.542.37.843.471.66.222.853-.369.989-.985.165-.747.21-1.522.189-2.29.02-.768-.024-1.543-.19-2.29"></path>`
            break
          case "Beast":
            color = "beast"
            logo = `<path d="M7.933 4.886a1.91 1.91 0 100-3.82 1.91 1.91 0 000 3.82M12.713 2.635a1.91 1.91 0 100 3.82 1.91 1.91 0 000-3.82M5.064 4.544a1.91 1.91 0 10-3.82 0 1.91 1.91 0 003.82 0M7.916 6.11a4.487 4.487 0 100 8.972 4.487 4.487 0 000-8.973"></path>`
            break
          case "Dusk":
            color = "hidden-3"
            logo = `<path d="M7.63001266,1.62274177 C8.88782226,1.51583444 10.1636395,1.78756482 11.2673834,2.40169006 C11.8081596,2.7023783 12.3047352,3.08210876 12.7376473,3.52467232 C12.9490102,3.74103673 13.1452758,3.97233538 13.3242613,4.21638278 C13.4370367,4.36991408 13.5407173,4.54329701 13.6156584,4.71831906 C13.6345756,4.76239329 13.6573126,4.81174914 13.6616781,4.86019437 C13.6693177,4.94178634 13.6385773,4.93595834 13.5714577,4.89625511 C13.3972015,4.79371878 13.2387703,4.66732408 13.0688796,4.55823125 C12.8409641,4.41162069 12.5972237,4.28923274 12.3431153,4.19434566 C11.8178001,3.99783287 11.2510128,3.92152253 10.6924107,3.96905713 C8.59278724,4.14735743 7.03521333,5.99592539 7.21310744,8.09819336 C7.39118345,10.2004613 9.23760792,11.759997 11.3370495,11.5816967 C11.8849197,11.5352549 12.4202391,11.3704318 12.8982613,11.0985193 C13.1296328,10.9664787 13.3449974,10.8100334 13.5450828,10.6342829 C13.6867797,10.5097094 13.8115602,10.3864109 13.9381597,10.2479959 C13.9567131,10.2275979 14.0356559,10.1600296 14.0416584,10.2235912 C14.0487524,10.3000837 14.0414765,10.3714766 14.0291076,10.4474227 C13.9960026,10.6508563 13.9345218,10.8337097 13.8461204,11.020752 C13.7593561,11.2048803 13.6558574,11.3806308 13.5419906,11.5487321 C13.3066174,11.8960443 13.0272254,12.2125775 12.7263697,12.5036131 C12.4991819,12.7234379 12.2585337,12.929239 12.0062442,13.1193774 C11.0474713,13.8413206 9.90280093,14.2685857 8.71011004,14.3767679 C5.19260867,14.6752707 2.09947025,12.0625065 1.80097921,8.54057479 C1.50285196,5.01864306 4.11251129,1.92142664 7.63001266,1.62274177 Z M11.5244204,5.52724511 C11.6468362,5.52724511 12.0266347,6.32950539 12.3043896,6.95546875 C12.9326578,7.23320925 13.7011676,7.59581997 13.7011676,7.70673405 C13.7011676,7.818923 12.9159234,8.18827234 12.2838354,8.4671056 C12.005171,9.09980758 11.6362863,9.88622299 11.5244204,9.88622299 C11.4136458,9.88622299 11.0516731,9.11692732 10.7739182,8.48768571 C10.1487423,8.20976308 9.34749118,7.82948625 9.34749118,7.70673405 C9.34749118,7.58543885 10.1312803,7.21208276 10.753364,6.93488863 C11.0302094,6.3120214 11.4030958,5.52724511 11.5244204,5.52724511 Z"></path>`
            break
          case "Bug":
            color = "bug"
            logo = `<path d="M13.264 3.198c-1.565.33-3.077.994-4.084 2.216A6.19 6.19 0 008 7.704a6.19 6.19 0 00-1.18-2.29C5.813 4.192 4.301 3.527 2.736 3.198c-.357-.075-.75-.181-1.116-.197-.72-.031-.612.948-.616 1.439-.006.666.103 1.328.249 1.979.171.764.311 1.571.851 2.193.356.41.846.692 1.321.972.083.048.169.1.213.183.05.095.033.208.016.313l-.383 2.245c-.029.168-.048.366.081.484a.53.53 0 00.239.105c1.534.367 3.211-.478 4.01-1.774.176-.286.305-.585.399-.893.094.308.223.607.399.893.799 1.296 2.476 2.141 4.01 1.774a.529.529 0 00.238-.105c.13-.118.11-.316.082-.484l-.383-2.245c-.018-.105-.034-.218.016-.313.044-.082.13-.135.212-.183.476-.28.966-.561 1.322-.972.54-.622.68-1.43.851-2.193.146-.65.255-1.313.25-1.98-.005-.49.104-1.47-.617-1.438-.366.016-.759.122-1.116.197z"></path>`
            break
          case "Mech":
            color = "hidden-1"
            logo = `<path d="M9.23004444,9.8361067 C9.52195556,9.87521781 9.84391111,9.86935115 10.1345778,9.83539559 C10.0403556,9.98632892 9.84337778,10.0949511 9.69511111,10.1827734 C9.31342222,10.4090845 8.85333333,10.4679289 8.41653333,10.4544178 C8.07502222,10.4437511 7.73671111,10.3552178 7.43111111,10.2032178 C7.12924444,10.0533511 6.8624,9.88588448 6.65102222,9.61815115 C6.64888889,9.61548448 6.64746667,9.61192892 6.64622222,9.60855115 C6.632,9.59504003 6.61688889,9.58312892 6.60284444,9.56926226 C5.79484444,8.76126226 5.79484444,7.45139559 6.60284444,6.64339559 C6.69173333,6.55468448 6.78737778,6.47735115 6.88675556,6.40819559 C6.59608889,6.36979559 6.27591111,6.37548448 5.98648889,6.40944003 C6.08071111,6.2585067 6.27768889,6.1497067 6.42595556,6.06206226 C6.80782222,5.83557337 7.26791111,5.7769067 7.70453333,5.79024003 C8.04604444,5.8009067 8.38435556,5.88961781 8.68995556,6.04144003 C8.92711111,6.1593067 9.14222222,6.28872892 9.32622222,6.4681067 C9.39626667,6.52197337 9.46435556,6.57939559 9.52871111,6.64339559 C10.3367111,7.45139559 10.3367111,8.76126226 9.52871111,9.56926226 C9.42791111,9.66988448 9.31964444,9.75806226 9.20533333,9.83344003 C9.21333333,9.83432892 9.22204444,9.83521781 9.23004444,9.8361067 M11.1130667,2.74437337 C10.5210667,2.39806226 5.6112,2.40944003 5.008,2.74437337 C4.4048,3.07948448 1.95555556,7.00215115 1.95555556,8.03166226 C1.95555556,9.06117337 4.41617778,12.8983289 5.008,13.3185956 C5.6,13.7388623 10.5098667,13.7280178 11.1130667,13.3185956 C11.7160889,12.9091734 14.1655111,8.86881781 14.1655111,8.03166226 C14.1655111,7.1945067 11.7050667,3.09104003 11.1130667,2.74437337"></path>`
            break
          default:
            color = axie.class
            logo = ``
            break
        }

        let html = `
        <div class="m-8 cursor-pointer ${axie.class}">
          <a href="https://marketplace.axieinfinity.com/axie/${axie.id}/">
            <div class="AxieCard_AxieCard__1vv4n">
              <div class="border border-gray-3 bg-gray-4 rounded transition hover:shadow hover:border-gray-6">
                <div class="px-12 py-16">
                  <div class="flex leading-16 items-center justify-between">
                    <div class="flex AxieCard_TagContainer__3Kjeq">
                      <div><span class="flex px-8 rounded text-12  border border-transparent"
                          style="padding-top: 1px; padding-bottom: 1px; background-color: var(--color-${color}); border-color: transparent;">
                          <span class="truncate">#${axie.id}</span></span></div>
                    </div>
                  </div>
                  <div class="mt-8 mb-4 flex flex-row">
                    <div><svg width="16" height="16" viewBox="0 0 16 16" style="fill: var(--color-${color});">
                        ${logo}
                      </svg></div><small class="truncate ml-4">${axie.name}</small>
                  </div><small class="block text-gray-2 truncate">Breed count: ${axie.breedCount}</small>
                  <div class="flex justify-center items-center AxieCard_ImagePlaceholder__3rTDX"><img
                      src="${axie.image}" alt="${axie.id}">
                  </div>
                  <div class="h-0 pb-24 flex flex-row flex-wrap justify-center overflow-hidden items-baseline"></div>
                </div>
              </div>
            </div>
          </a>
        </div>
    `

        let divAxie = document.getElementById("axies")
        divAxie.innerHTML += html
      })
    })
    .catch((error) => {
      // const alertAxie = document.getElementById("alertAxie")
      // alertAxie.innerHTML = `<span style="font-size: 26px; color: red; padding: 5px 18px;">ERROR! hubo problema al cargar todos los axies, por
      //   favor vuelva a recargar la pagina.</span>`
      getAxieByRonin(ronin)
    })
}

const getAxiesPromises = async () => {
  await Promise.all([
    getAxieByRonin("a68713b25784d161701b1f24b83381a2b94d0c5d"),
    getAxieByRonin("0e2232849d4f1f61c363c16adc0d50a828c0c56d"),
    getAxieByRonin("d96800e82f002213e0b7a23410408742bf82efba"),
    getAxieByRonin("8d7a878cfcbcf73a29108132ff1f49df10b3b5fa"),
    getAxieByRonin("0a29c4022455bbd4cda3c9ed47b7e29ce7bc79bc"),
    getAxieByRonin("d6e866e51e1ba05c9a6dedd08a988a119180f41d"),
    getAxieByRonin("facbe4eff2ac9ae6bd9e5956e74ae186faaf93a9"),
    getAxieByRonin("76e7e6978bbe201997625f9849e1a8baa48df628"),
    getAxieByRonin("0fabff9a41458174119c950820efd895f6018053"),
    getAxieByRonin("21de833d52ae71054c244d034e8ac7fe752a13c1"),
    getAxieByRonin("bb0e6e22fcdb594b2ef3dad66f4f8881b678b6df"),
    getAxieByRonin("4e3801c7bc8f521e798124de53677002d7c29b67"),
    getAxieByRonin("87a86ceb2d290b4ed69e844299ff973875375e78"),
    getAxieByRonin("b9913f518b1bed1b43d476cf76b86c65562ed609"),
    getAxieByRonin("0b05ae8343238e270fdb52e4b28375226efdf02c"),
    getAxieByRonin("9f651bb71feab690a3cbc6d4fe0537c7aeb0578e")
  ])

  document.getElementById("axies").style.display = "flex"
  document.getElementById("loading").style.display = "none"
}

getAxiesPromises()

let filterAll = []
let filterComparative = [
  "Beast",
  "Aquatic",
  "Plant",
  "Bug",
  "Bird",
  "Reptile",
  "Mech",
  "Dawn",
  "Dusk",
]

document.querySelectorAll(".ClassFilter_item__3kQTw").forEach((item) => {
  item.addEventListener("click", (event) => {
    let filter = event.target.innerText

    let divFilter = document.getElementById(`filter-${filter}`)
    let listClass = divFilter.className.split(" ")

    listClass.map((item) => {
      if (item === "bg-gray-3") {
        divFilter.removeAttribute
        listClass.pop()
        listClass.push("bg-primary-4")

        filterAll.push(filter)
      }

      if (item === "bg-primary-4") {
        divFilter.removeAttribute
        listClass.pop()
        listClass.push("bg-gray-3")

        filterAll = filterAll.filter((i) => i != filter)
      }
    })
    divFilter.className = listClass.join(" ")

    const hiddenDiv = filterComparative.filter((x) => !filterAll.includes(x))

    console.log(hiddenDiv)

    if (filterAll.length == 0) {
      filterComparative.map((item) => {
        var elems = document.getElementsByClassName(item)
        for (var i = 0; i < elems.length; i += 1) {
          elems[i].style.display = "block"
        }
      })
    } else {
      filterComparative.map((item) => {
        var elems = document.getElementsByClassName(item)
        for (var i = 0; i < elems.length; i += 1) {
          elems[i].style.display = "block"
        }
      })

      hiddenDiv.map((item) => {
        var elems = document.getElementsByClassName(item)
        for (var i = 0; i < elems.length; i += 1) {
          elems[i].style.display = "none"
        }
      })
    }
  })
})
